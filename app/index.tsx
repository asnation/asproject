import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {App} from './App/App';

/**
 * Каркас приложения с подключением Redux.
 */
ReactDOM.render(
    <App/>,
    document.getElementById('app')
);
